from django.http import HttpResponse

# We define a function that called index
# @param request: (must) To capture all the request that sent to our website
# @return HTTP response 
def index(request):
    return HttpResponse("Hello, world. You're at the application index.")
